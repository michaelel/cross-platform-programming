export enum GroupsEnum {
    FAMILY = 'Семья',
    WORK = 'Работа',
    FRIENDS = 'Друзья',
}
