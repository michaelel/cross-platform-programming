import {AddressInterface} from './address.interface';
import {GroupsEnum} from './groups.enum';

export interface PersonalInfoInterface {
    id: string | number;
    secondName: string;
    firstName: string;
    patronymic: string;
    shortName: string;
    nickname: string;
    comment: string;
    groups: GroupsEnum[];
    homePhone: string;
    mobilePhone: string;
    secondMobilePhone?: string;
    email: string;
    skype: string;
    address: AddressInterface;
    fullAddress: string;
    dateCreate: string;
    dateEdit: string;
}

