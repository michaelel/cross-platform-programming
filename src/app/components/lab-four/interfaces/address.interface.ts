export interface AddressInterface {
    index: number | string;
    city: string;
    street: string;
    building: string;
    flat?: string;
}
