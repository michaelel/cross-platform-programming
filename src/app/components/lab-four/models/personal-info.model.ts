import {GroupsEnum} from '../interfaces/groups.enum';
import {AddressInterface} from '../interfaces/address.interface';

export class PersonalInfoModel {
    constructor(
        public id: string | number,
        public secondName: string,
        public firstName: string,
        public patronymic: string,
        public shortName: string,
        public nickname: string,
        public comment: string,
        public groups: GroupsEnum[],
        public homePhone: string,
        public mobilePhone: string,
        public email: string,
        public skype: string,
        public address: AddressInterface,
        public fullAddress: string,
        public dateCreate: string,
        public dateEdit: string,
        public secondMobilePhone?: string,
    ) {}
}
