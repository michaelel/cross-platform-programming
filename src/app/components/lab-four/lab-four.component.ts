import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GroupsEnum} from './interfaces/groups.enum';
import {MatCheckboxChange, MatSelectChange} from '@angular/material';
import {PersonalInfoInterface} from './interfaces/personal-info.interface';
import {PersonalInfoModel} from './models/personal-info.model';
import * as moment from 'moment';

@Component({
    selector: 'app-lab-four',
    templateUrl: './lab-four.component.html',
    styleUrls: ['./lab-four.component.sass']
})
export class LabFourComponent implements OnInit {
    personalInfoForm: FormGroup;
    addressForm: FormGroup;

    noteBook: PersonalInfoInterface[] = [];

    groups = GroupsEnum;

    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    titlesRegex = /^[A-ZА-Я][a-zа-я]+$/;
    onlyNumbersRegex = /^[0-9]+$/;

    mobilePhoneRegex = /^\+?[0-9]+$/;

    activeNote: PersonalInfoInterface;

    editMode: Boolean;

    familyChecked: boolean;
    workChecked: boolean;
    friendsChecked: boolean;

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.createPersonalForm();
        this.groupsCtrl.setValue([]);
        this.createAddressForm();
        this.createTestNote();
    }

    createPersonalForm(): void {
        this.personalInfoForm = this.fb.group({
            secondName: [
                '',
                [
                    Validators.required,
                    Validators.pattern(this.titlesRegex),
                ]
            ],
            firstName: [
                '',
                [
                    Validators.required,
                    Validators.pattern(this.titlesRegex),
                ]
            ],
            patronymic: [
                '',
                [
                    Validators.required,
                    Validators.pattern(this.titlesRegex),
                ]
            ],
            nickname: [
                '',
                Validators.required,
            ],
            comment: [
                '',
                Validators.required,
            ],
            groups: [],
            homePhone: [
                '',
                [
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(7),
                    Validators.pattern(this.onlyNumbersRegex),
                ],
            ],
            mobilePhone: [
                '',
                [
                    Validators.required,
                    Validators.minLength(10),
                    Validators.maxLength(13),
                    Validators.pattern(this.mobilePhoneRegex)
                ],
            ],
            secondMobilePhone: [
                '',
                [
                    Validators.minLength(10),
                    Validators.maxLength(14),
                    Validators.pattern(this.mobilePhoneRegex)
                ]
            ],
            email: [
                '',
                [Validators.required, Validators.pattern(this.emailRegex)],
            ],
            skype: [
                '',
                Validators.required,
            ],
        });
    }

    createAddressForm(): void {
        this.addressForm = this.fb.group({
            index: [
                '',
                [
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(5),
                ],
            ],
            city: [
                '',
                [
                    Validators.pattern(this.titlesRegex),
                    Validators.required,
                ]
            ],
            street: [
                '',
                [
                    Validators.pattern(this.titlesRegex),
                    Validators.required,
                ],
            ],
            building: [
                '',
                Validators.required,
            ],
            flat: [''],
        });
    }

    changeGroups(event: MatCheckboxChange): void {
        event.checked
            ? this.groupsCtrl.value.push(this.groups[event.source.value])
            : this.groupsCtrl.setValue(this.groupsCtrl.value.filter(group => group !== this.groups[event.source.value]));
    }

    createNote(): void {
        const resNote = {
            id: this.noteBook.length,
            ...this.personalInfoForm.value,
            address: this.addressForm.value,
            fullAddress: this.getFullAddress(),
            shortName: this.shortName,
            groups: this.groupsCtrl.value,
            dateCreate: moment().format('HH:mm DD/MM/YY'),
            dateEdit: moment().format('HH:mm DD/MM/YY'),
        };
        this.noteBook.push(this.createModel(resNote));
        window.alert('Запись успешно создана');
        this.cancel();
        this.activeNote = this.noteBook[this.noteBook.length - 1];
    }

    editNote(): void {
        this.editMode = true;
        this.personalInfoForm.setValue({
            firstName: this.activeNote.firstName,
            secondName: this.activeNote.secondName,
            patronymic: this.activeNote.patronymic,
            nickname: this.activeNote.nickname,
            comment: this.activeNote.comment,
            groups: this.activeNote.groups,
            homePhone: this.activeNote.homePhone,
            mobilePhone: this.activeNote.mobilePhone,
            secondMobilePhone: this.activeNote.secondMobilePhone,
            email: this.activeNote.email,
            skype: this.activeNote.skype,
        });

        this.addressForm.setValue({
            index: this.activeNote.address.index,
            city: this.activeNote.address.city,
            street: this.activeNote.address.street,
            building: this.activeNote.address.building,
            flat: this.activeNote.address.flat,
        });
        this.familyChecked = !!this.activeNote.groups.filter(group => group === this.groups.FAMILY).length;
        this.workChecked = !!this.activeNote.groups.filter(group => group === this.groups.WORK).length;
        this.friendsChecked = !!this.activeNote.groups.filter(group => group === this.groups.FRIENDS).length;
    }

    saveEditedNote(): void {
        this.noteBook.map((note, index) => {
            if (note.id === this.activeNote.id) {
                this.noteBook[index] = {
                    ...this.activeNote,
                    ...this.personalInfoForm.value,
                    address: this.addressForm.value,
                    fullAddress: this.getFullAddress(),
                    shortName: this.shortName,
                    groups: this.groupsCtrl.value,
                    dateEdit: moment().format('HH:mm DD/MM/YY'),
                };
                this.activeNote = this.noteBook[index];
            }
        });
        window.alert('Данные успешно отредактированы');
        this.cancel();
    }

    removeNote(): void {
        this.noteBook = this.noteBook.filter(note => note.id !== this.activeNote.id);
        this.activeNote = null;
        window.alert('Запись успешно удалена');
    }

    cancel(): void {
        this.personalInfoForm.reset();
        this.addressForm.reset();
        this.editMode = false;
        this.familyChecked = false;
        this.workChecked = false;
        this.friendsChecked = false;
    }

    changeNote(event: any): void {
        this.activeNote = this.noteBook[event];
    }

    createTestNote(): void {
        const resNote = {
            id: this.noteBook.length,
            secondName: 'sdfsdf',
            firstName: 'sdfgtret',
            patronymic: 'sfsdfre',
            shortName: 'Asfs. S',
            nickname: 'asdfdsf',
            comment: 'sdfdsfs',
            groups: [],
            homePhone: '1243242',
            mobilePhone: '3423422323',
            secondMobilePhone: '3423422323',
            email: 'fdsdf@safdsf.dsf',
            skype: 'sdfsdf',
            address: {
                index: '12434',
                city: 'dsfsdf',
                street: 'fewfwef',
                building: '3',
                flat: '',
            },
            fullAddress: 'sdfsdfsd',
            dateCreate: moment().format('HH:mm DD/MM/YY'),
            dateEdit: moment().format('HH:mm DD/MM/YY'),
        };
        this.noteBook.push(this.createModel(resNote));
    }

    private createModel(resNote: PersonalInfoInterface): PersonalInfoModel {
        return new PersonalInfoModel(
            resNote.id,
            resNote.secondName,
            resNote.firstName,
            resNote.patronymic,
            resNote.shortName,
            resNote.nickname,
            resNote.comment,
            resNote.groups,
            resNote.homePhone,
            resNote.mobilePhone,
            resNote.email,
            resNote.skype,
            resNote.address,
            resNote.fullAddress,
            resNote.dateCreate,
            resNote.dateEdit,
            resNote.secondMobilePhone,
        );
    }

    get firstNameCtrl(): AbstractControl {
        return this.personalInfoForm.get('firstName');
    }

    get secondNameCtrl(): AbstractControl {
        return this.personalInfoForm.get('secondName');
    }

    get indexCtrl(): AbstractControl {
        return this.addressForm.get('index');
    }

    get cityCtrl(): AbstractControl {
        return this.addressForm.get('city');
    }

    get streetCtrl(): AbstractControl {
        return this.addressForm.get('street');
    }

    get buildingCtrl(): AbstractControl {
        return this.addressForm.get('building');
    }

    get flatCtrl(): AbstractControl {
        return this.addressForm.get('flat');
    }

    get groupsCtrl(): AbstractControl {
        return this.personalInfoForm.get('groups');
    }

    getFullAddress(): string {
        console.log(this.addressForm);
        console.log(this.indexCtrl);
        console.log(this.cityCtrl);
        console.log(this.streetCtrl);
        console.log(this.buildingCtrl);
        console.log(this.flatCtrl);
        const flatData = this.flatCtrl.value && `, кв. ${this.flatCtrl.value}`;
        return this.indexCtrl.value + ', ' +
            this.cityCtrl.value + ', ' +
            this.streetCtrl.value + ', дом ' + this.buildingCtrl.value +
            flatData;
    }

    get shortName(): string {
        return this.secondNameCtrl.value[0].toUpperCase() +
            this.secondNameCtrl.value.slice(1) + ' ' +
            this.firstNameCtrl.value[0].toUpperCase() + '.';
    }

}
