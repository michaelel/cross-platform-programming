import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserInterface} from './interfaces/user.interface';

@Component({
  selector: 'app-exception',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.sass']
})
export class ExceptionComponent implements OnInit {

  loginForm: FormGroup;

  users: UserInterface[] = [
    {
      id: 1,
      login: 'qwe',
      password: '123'
    },
    {
      id: 2,
      login: 'ewq',
      password: '123',
    }
  ];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.loginForm = this.fb.group({
      login: [
          '',
          Validators.required,
      ],
      password: [
        '',
        Validators.required,
      ],
    });
  }

  clear(): void {
    this.loginForm.reset();
  }

  apply(): void {
    if (this.users.filter(user => user.login === this.loginCtrl.value).length) {
      window.alert('Данный логин уже существует, попробуйте ввести другие данные!');
      this.loginCtrl.reset();
    } else {
      this.users.push({ ...this.loginForm.value, id: Math.trunc(Math.random() * 1000) });
      window.alert('Регистрация успешна');
      this.loginForm.reset();
      this.loginForm.markAsDirty();
    }
  }

  get loginCtrl(): AbstractControl {
    return this.loginForm.get('login');
  }

  get passwordCtrl(): AbstractControl {
    return this.loginForm.get('password');
  }

}
