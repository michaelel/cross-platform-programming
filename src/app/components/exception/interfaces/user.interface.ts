export interface UserInterface {
    id: string | number;
    login: string;
    password: string;
}
