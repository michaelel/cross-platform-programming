import { Component, OnInit } from '@angular/core';
import {HelloAngularService} from './services/hello-angular.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-hello-angular',
  templateUrl: './hello-angular.component.html',
  styleUrls: ['./hello-angular.component.sass']
})
export class HelloAngularComponent implements OnInit {

  text: string;
  textCtrl: FormControl;

  constructor(private dataService: HelloAngularService) { }

  ngOnInit() {
    this.textCtrl = new FormControl('');
    this.dataService.text.subscribe(
        text => this.text = text,
        e => console.log(e),
    );
  }

  apply(): void {
    this.dataService.setText(this.textCtrl.value);
    this.textCtrl.reset();
  }

}
