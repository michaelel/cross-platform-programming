import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HelloAngularService {

    private _text: BehaviorSubject<string> = new BehaviorSubject<string>('');

    constructor() {
    }

    get text(): BehaviorSubject<string> {
        return this._text;
    }

    setText(text: string): void {
        this._text.next(text);
    }
}
