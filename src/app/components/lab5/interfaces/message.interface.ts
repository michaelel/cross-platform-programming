export interface MessageInterface {
    id: string | number;
    author: string;
    theme: string;
    text: string;
    dateCreate: string;
    dateEdit: string;
}
