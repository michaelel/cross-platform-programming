import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageInterface} from './interfaces/message.interface';

import * as moment from 'moment';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-lab5',
  templateUrl: './lab5.component.html',
  styleUrls: ['./lab5.component.sass']
})
export class Lab5Component implements OnInit {
  infoForm: FormGroup;

  displayedColumns: string[] = [
      'id',
      'author',
      'theme',
      'text',
      'dateCreate',
      'dateEdit',
      'actions',
  ];

  defaultTimeFormat = 'HH:mm:ss DD/MM/YYYY';

  messages: BehaviorSubject<MessageInterface[]> = new BehaviorSubject([
    {
      id: 12412,
      author: 'Michael',
      theme: 'Lab',
      text: 'Lab 5 completed',
      dateCreate: moment().format(this.defaultTimeFormat),
      dateEdit: moment().format(this.defaultTimeFormat),
    }
  ]);

  activeMessage: MessageInterface;

  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.infoForm = this.fb.group({
      author: [
          '',
          Validators.required,
      ],
      theme: [
          '',
          Validators.required,
      ],
      text: [
          '',
          Validators.required,
      ],
    });
  }

  createMessage(): void {
    this.messages.next([
        ...this.messages.getValue(),
        {
          id: Math.trunc(Math.random() * 10000),
          ...this.infoForm.value,
          dateCreate: moment().format(this.defaultTimeFormat),
          dateEdit: moment().format(this.defaultTimeFormat),
        }
    ]);
    this.cancel();
  }

  saveEditedMessage(): void {
    this.messages.next(this.messages.getValue().map(message => {
      if (message.id !== this.activeMessage.id) { return message; }

      return {
        ...message,
        ...this.infoForm.value,
        dateEdit:  moment().format(this.defaultTimeFormat),
      };
    }));
    this.cancel();
  }

  cancel(): void {
    this.infoForm.reset();
    this.activeMessage = null;
  }

  editMessage(message: MessageInterface): void {
    this.activeMessage = message;
    this.infoForm.patchValue(this.activeMessage);
    this.infoForm.markAsDirty();
  }

  removeMessage(message: MessageInterface): void {
    this.messages.next(this.messages.getValue().filter(item => item.id !== message.id));
  }

}
