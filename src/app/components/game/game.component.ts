import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit {

  gapStart = 0;
  gapEnd = 100;

  secretNumber = Math.trunc(Math.random() * 100);

  finishGame: boolean;

  numberCtrl: FormControl;

  constructor() { }

  ngOnInit() {
    this.numberCtrl = new FormControl(
        '',
        [
          Validators.required,
          Validators.min(this.gapStart),
          Validators.max(this.gapEnd),
        ],
    );
  }

  guess(): void {
    if (this.secretNumber > +this.numberCtrl.value) {
      window.alert('Больше');
      this.gapStart = +this.numberCtrl.value + 1;
      this.setValidators();
    } else if (this.secretNumber < +this.numberCtrl.value) {
      window.alert('Меньше');
      this.gapEnd = +this.numberCtrl.value - 1;
      this.setValidators();
    } else {
      window.alert('Вы выиграли. Поздравляю!!!');
      this.numberCtrl.disable();
      this.finishGame = true;
    }
  }

  restart(): void {
    this.gapStart = 0;
    this.gapEnd = 100;
    this.numberCtrl.enable();
    this.finishGame = false;
    this.setValidators();
  }

  setValidators(): void {
    this.numberCtrl.reset();
    this.numberCtrl.clearValidators();
    this.numberCtrl.setValidators([
      Validators.required,
      Validators.min(this.gapStart),
      Validators.max(this.gapEnd),
    ]);
  }

  get title(): string {
    return this.finishGame
        ? 'Вы выиграли. Поздравляю!!! Чтобы сыграть ее раз, нажмите на кнопку "Начать заново"'
        : `Введите число в промежутке от ${this.gapStart} до ${this.gapEnd}`;
  }
}
