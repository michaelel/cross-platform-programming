export interface BookInterface {
    id: string | number;
    name: string;
    author: string;
    publishing: string;
    publishYear: string;
    price: string;
}
