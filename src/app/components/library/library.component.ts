import {Component, OnInit} from '@angular/core';
import {BookInterface} from './interfaces/book.interface';
import {BehaviorSubject} from 'rxjs';
import {FormBuilder, FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-library',
    templateUrl: './library.component.html',
    styleUrls: ['./library.component.sass']
})
export class LibraryComponent implements OnInit {
    removeBookIndexCtrl: FormControl;

    library: BehaviorSubject<any> = new BehaviorSubject({
        1111: {
            name: 'Стив Джобс',
            author: 'Уолтер Айзексон',
            publishing: 'Simon & Schuster',
            publishYear: '2011',
            price: '500',
        },
        1123: {
            name: 'Робинзон крузо',
            author: 'Даниель Дефо',
            publishing: 'Clever',
            publishYear: '1719',
            price: '200',
        },
        5555: {
            name: 'Собака Баскервилей',
            author: 'Артур Конан Дойл ',
            publishing: 'Lalala',
            publishYear: '1902',
            price: '400',
        },
    });

    libraryKeys: string[];

    dataSource: BookInterface[] = [];

    displayedColumns = [
        'id',
        'name',
        'author',
        'publishing',
        'publishYear',
        'price',
    ];

    libraryArray = [];

    activeBookIndex: BehaviorSubject<number> = new BehaviorSubject(null);

    constructor() {
        this.libraryKeys = Object.keys(this.library);
    }

    ngOnInit() {
        this.library.subscribe(
            res => {
                this.libraryKeys = Object.keys(res);
                this.activeBookIndex.next(null);
                this.libraryArray = [];
                this.libraryKeys.map(key => this.libraryArray.push({ ...res[key], id: key }));
            }
        );
        this.removeBookIndexCtrl = new FormControl('', Validators.required);
        this.activeBookIndex.subscribe(
            i => this.dataSource = [this.libraryArray[i]],
        );

    }

    show(): void {
        this.activeBookIndex.next(0);
    }

    next(): void {
        this.activeBookIndex.next(this.activeBookIndex.getValue() + 1);
    }

    prev(): void {
        this.activeBookIndex.next(this.activeBookIndex.getValue() - 1);
    }

    remove(): void {
        if (this.library.getValue()[this.removeBookIndexCtrl.value]) {
            const resLibrary = this.library.getValue();
            delete resLibrary[this.removeBookIndexCtrl.value];
            this.library.next(resLibrary);
            // delete this.library.getValue()[this.removeBookIndexCtrl.value];
            window.alert('Книга удалена');
        } else {
            window.alert('Книга не найдена');
        }
    }

    sortByName(): void {
        this.libraryArray.sort((a, b) => {
            if (a.name < b.name) { return -1; }
            if (a.name > b.name) { return 1; }
            return 0;
        });
        this.activeBookIndex.next(null);
    }

    sortByPublishYear(): void {
        this.libraryArray.sort((a, b) => {
            if (a.publishYear < b.publishYear) { return -1; }
            if (a.publishYear > b.publishYear) { return 1; }
            return 0;
        });
        this.activeBookIndex.next(null);
    }

    sortByAuthor(): void {
        this.libraryArray.sort((a, b) => {
            if (a.author < b.author) { return -1; }
            if (a.author > b.author) { return 1; }
            return 0;
        });
        this.activeBookIndex.next(null);
    }
}
