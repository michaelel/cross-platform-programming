import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-calculate-replay-item-count-in-array',
  templateUrl: './calculate-replay-item-count-in-array.component.html',
  styleUrls: ['./calculate-replay-item-count-in-array.component.sass']
})
export class CalculateReplayItemCountInArrayComponent implements OnInit {

  resStructure: any = {};
  arrayCtrl: FormControl;
  resStructureKeys: string[];

  constructor() { }

  ngOnInit() {
    this.arrayCtrl = new FormControl('');
  }

  calculate(): void {
    this.resStructure = {};
    this.resStructureKeys = [];
    const resArray = this.arrayCtrl.value.split(',');
    resArray.map(i => this.resStructure[i] = this.resStructure[i] ? this.resStructure[i] + 1 : 1);
    this.resStructureKeys = Object.keys(this.resStructure);
  }

}
