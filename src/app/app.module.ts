import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloAngularComponent } from './components/hello-angular/hello-angular.component';
import {HelloAngularService} from './components/hello-angular/services/hello-angular.service';
import {MaterialModule} from './shared/modules/material/material.module';
import { LabFourComponent } from './components/lab-four/lab-four.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CalculateReplayItemCountInArrayComponent } from './components/calculate-replay-item-count-in-array/calculate-replay-item-count-in-array.component';
import { Lab5Component } from './components/lab5/lab5.component';
import { LibraryComponent } from './components/library/library.component';
import { GameComponent } from './components/game/game.component';
import { ExceptionComponent } from './components/exception/exception.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloAngularComponent,
    LabFourComponent,
    CalculateReplayItemCountInArrayComponent,
    Lab5Component,
    LibraryComponent,
    ExceptionComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [
      HelloAngularService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
